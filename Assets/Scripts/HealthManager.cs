﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    public int maxHealth;
    public int currentHealth;

    public float invisibilityLength;
    private float invisibilityCounter;
    private float flashCounter;
    public float flashLength = .1f;

    public Renderer playerRenderer;

    public PlayerController thePlayer;

    private bool isRespawnning;
    private Vector3 respawnPoint;
    public float respawnLength;

    public GameObject deathEffect;
    public Image blackScreen;
    private bool isFadeToBlack;
    private bool isFadeFromBlack;
    public float fadeSpeed;
    public float waitForFade;

    public Text healthText;

    // Start is called before the first frame update
    void Start()
    {
        //thePlayer = FindObjectOfType<PlayerController>();
        currentHealth = maxHealth;
        respawnPoint = thePlayer.transform.position;
        healthText.text = "Health: " + currentHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if(invisibilityLength > 0)
        {
            invisibilityCounter -= Time.deltaTime;

            flashCounter -= Time.deltaTime;
            if(flashCounter <= 0)
            {
                playerRenderer.enabled = !playerRenderer.enabled;

                flashCounter = flashLength;
            }

            if(invisibilityCounter <= 0)
            {
                playerRenderer.enabled = true;
            }
        }

        if(isFadeToBlack)
        {
            blackScreen.color = new Color(blackScreen.color.r, blackScreen.color.g, blackScreen.color.b, Mathf.MoveTowards(blackScreen.color.a, 1f, fadeSpeed * Time.deltaTime));
            if(blackScreen.color.a == 1f)
            {
                isFadeToBlack = false;
            }
        }

        if (isFadeFromBlack)
        {
            blackScreen.color = new Color(blackScreen.color.r, blackScreen.color.g, blackScreen.color.b, Mathf.MoveTowards(blackScreen.color.a, 0f, fadeSpeed * Time.deltaTime));
            if (blackScreen.color.a == 0f)
            {
                isFadeFromBlack = false;
            }
        }
    }

    public void HurtPlayer(int damage, Vector3 direction)
    {

        if (invisibilityCounter <= 0)
        {
            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                Respawn();
            }
            else
            {
                thePlayer.knockBack(direction);

                invisibilityCounter = invisibilityLength;

                playerRenderer.enabled = false;
                flashCounter = flashLength;
            }

        }
        healthText.text = "Health: " + currentHealth;
    }

    public void HealPlayer(int healAmount)
    {
        currentHealth += healAmount;

        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        healthText.text = "Health: " + currentHealth;
    }

    public void Respawn()
    {
        // thePlayer.transform.position = respawnPoint;
        // currentHealth = maxHealth;
        if (!isRespawnning)
        {
            StartCoroutine("RespawnCo");
        }
    }

    public IEnumerator RespawnCo() // own loop of time out of Unity kinda
    {
        isRespawnning = true;
        thePlayer.gameObject.SetActive(false);
        Instantiate(deathEffect, thePlayer.transform.position, thePlayer.transform.rotation);

        yield return new WaitForSeconds(respawnLength);

        isFadeToBlack = true;

        yield return new WaitForSeconds(waitForFade);

        isFadeToBlack = false;
        isFadeFromBlack = true;


        isRespawnning = false;

        thePlayer.gameObject.SetActive(true);

        thePlayer.transform.position = respawnPoint;

        currentHealth = maxHealth;

        invisibilityCounter = invisibilityLength;
        playerRenderer.enabled = false;
        flashCounter = flashLength;
    }

    public void SetSpawnPoint(Vector3 newPosition)
    {
        respawnPoint = newPosition;
    }
}
