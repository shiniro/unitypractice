﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public Transform pivot;

    public Vector3 offset;

    public bool useOffsetValue;
    public bool invertY;

    public float rotateSpeed;
    public float maxViwAngle;
    public float minViewAngle;

    // Start is called before the first frame update
    void Start()
    {
        if (useOffsetValue)
        {
            // if traget was a GameObject and not Transform, to access position => target.transform.postion
            offset = target.position - transform.position;
        }

        pivot.position = target.position;
        // pivot.parent = target;
        pivot.parent = null;

        // lock the cursor at the center of the window
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update after Update (after something happen, example when the player move)
    void LateUpdate()
    {
        pivot.position = target.position;

        //Get the x position of the mouse & rotate the target
        float horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
        pivot.Rotate(0, horizontal, 0);

        //Get the y position of the mouse & rotate the pivot
        float vertical = Input.GetAxis("Mouse Y") * rotateSpeed;
        if (invertY)
        {
            pivot.Rotate(vertical, 0, 0); //-vertical so move up the mouse -> looks up
        } else
        {
            pivot.Rotate(-vertical, 0, 0); //-vertical so move up the mouse -> looks up
        }
        

        // Limit up/down camera rotation
        if (pivot.rotation.eulerAngles.x > maxViwAngle && pivot.rotation.eulerAngles.x < 180f)
        {
            pivot.rotation = Quaternion.Euler(maxViwAngle, 0, 0);
        }

        if (pivot.rotation.eulerAngles.x > 180f && pivot.rotation.eulerAngles.x < 360f + minViewAngle)
        {
            pivot.rotation = Quaternion.Euler(360f + minViewAngle, 0, 0);
        }

        // Move the camera based on the postion of the target & the original offset
        float desiredAngleY = pivot.eulerAngles.y;
        float desiredAngleX = pivot.eulerAngles.x;

        Quaternion rotation = Quaternion.Euler(desiredAngleX, desiredAngleY, 0);
        transform.position = target.position - (rotation * offset);

        // transform.position = target.position - offset;

        if (transform.position.y < target.position.y)
        {
            transform.position = new Vector3(transform.position.x, target.position.y - .5f, transform.position.z);
        }

        transform.LookAt(target);
    }
}
