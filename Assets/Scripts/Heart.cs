﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    public HealthManager theHealManager;
    public int heallingPoint;

    // Start is called before the first frame update
    void Start()
    {
        theHealManager = FindObjectOfType<HealthManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            theHealManager.HealPlayer(heallingPoint);

            Destroy(gameObject);
        }
    }
}
