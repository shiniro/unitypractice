﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public HealthManager theHealManager;

    public Renderer theRenderer;

    public Material checkPointOff;
    public Material checkPointOn;

    private Checkpoint[] checkpoints;

    // Start is called before the first frame update
    void Start()
    {
        theHealManager = FindObjectOfType<HealthManager>();
        checkpoints = FindObjectsOfType<Checkpoint>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CheckPointOn()
    {
        foreach(Checkpoint cp in checkpoints)
        {
            cp.CheckPointOff();
        }

        theRenderer.material = checkPointOn;
    }

    public void CheckPointOff()
    {
        theRenderer.material = checkPointOff;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Player"))
        {
            theHealManager.SetSpawnPoint(transform.position);
            CheckPointOn();
        }
    }
}
